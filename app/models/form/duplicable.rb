# frozen_string_literal: true

module Form::Duplicable
  extend ActiveSupport::Concern

  def duplicate!
    ActiveRecord::Base.transaction do
      form_duplicated = Form::Duplicate.new(self).tap(
        &:duplicate_form_and_children
      ).duplicated_form

      form_duplicated.save
      form_duplicated
    end
  end
end
