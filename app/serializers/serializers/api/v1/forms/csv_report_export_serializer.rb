# frozen_string_literal: true

module Api
  module V1
    module Forms
      class CsvReportExportSerializer
        include JSONAPI::Serializer

        attributes :action_cable_channel, :action_cable_channel_unique_id
      end
    end
  end
end
