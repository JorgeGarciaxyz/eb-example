# frozen_string_literal: true

require "active_model_errors_helpers"

module Builders
  module FormResponse
    # ResponseCreator
    class ResponseCreator < ::ApplicationService
      include ::Builders::FormResponse::ErrorsStructure

      private

      attr_accessor :answer_results
      attr_reader :answer_objects, :response

      def initialize(response, answer_objects)
        super()
        @response = response
        @answer_objects = answer_objects
        @answer_results = {}
        @errors = []
      end

      def call
        ::ActiveRecord::Base.transaction do
          response.save!
          process_answers_creation
          raise ActiveRecord::Rollback unless success
        end
        return handle_success if success?

        handle_errors
      rescue ActiveRecord::RecordInvalid => e
        add_formated_errors(errors: e.message)
        handle_errors_flow
      end

      def process_answers_creation
        import(:answer_text)
        import(:answer_choice)
        self.success = all_imports_succeeded?
      end

      def import(type)
        model_from_type = "::#{type.to_s.camelize}".constantize
        answer_results[type] = model_from_type.import(
          answer_objects[type], validates: true, all_or_none: true
        )
      end

      def all_imports_succeeded?
        failed_imports = answer_results.dig(:answer_text).failed_instances +
                         answer_results.dig(:answer_choice).failed_instances

        failed_imports.blank?
      end

      def handle_success
        self.success = true
        self.result = response
      end

      def handle_errors
        answer_results.each do |_key, values|
          values.failed_instances.each do |obj|
            add_formated_errors(**::ActiveModelErrorsHelpers.attributes_with_errors(obj))
          end
        end
      end
    end
  end
end
