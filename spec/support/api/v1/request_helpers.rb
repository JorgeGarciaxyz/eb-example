# frozen_string_literal: true

require "rspec/json_expectations"

module Api
  module V1
    module RequestHelpers
      def json_response
        @json_response ||= JSON.parse(response.body) || {}
      end
    end

    # rubocop:disable Metrics/AbcSize, Metrics/ModuleLength
    module ResponseFormat
      def match_form_csv_report_response_body(record)
        expect(record).to be_key("data")
        expect(record.dig("data", "attributes")).to be_key("action_cable_channel")
        expect(
          record.dig("data", "attributes")
        ).to be_key("action_cable_channel_unique_id")
      end

      def match_errors_response_body(record)
        expect(record).to be_key("errors")
      end

      def match_form_response_body(record)
        expect(record).to be_key("data")
        expect(record.dig("data", "attributes")).to be_key("secondary_organization_id")
        expect(record.dig("data", "attributes")).to be_key("name")
        expect(record.dig("data", "attributes")).to be_key("status")
        expect(record.dig("data", "attributes")).to be_key("close_date")
        expect(record.dig("data", "attributes")).to be_key("url")
        expect(record.dig("data", "attributes")).to be_key("settings")
        expect(record.dig("data", "attributes")).to be_key("unique_id")
        expect(record.dig("data", "relationships", "elements")).to be_key("links")
      end

      def match_forms_response_body(records)
        match_records(records, :match_form_response_body)
      end

      def match_element_response_body(record)
        expect(record).to be_key("data")
        expect(record.dig("data", "attributes")).to be_key("title")
        expect(record.dig("data", "attributes")).to be_key("position")
        expect(record.dig("data", "attributes")).to be_key("config")
        expect(record.dig("data", "attributes")).to be_key("element_type")
        expect(record.dig("data", "attributes")).to be_key("title_edited_with_answers")
        expect(record.dig("data", "relationships")).to be_key("form")
        expect(record.dig("data", "relationships", "element_choices")).to be_key("links")
      end

      def match_included_element_choices_response_body(record)
        expect(record.dig("included")[0]).to be_key("id")
        expect(record.dig("included")[0]).to be_key("type")
        expect(record.dig("included")[0].dig("type")).to eql("element_choice")
        expect(record.dig("included")[0]).to be_key("attributes")
        expect(record.dig("included")[0].dig("attributes")).to be_key("title")
      end

      def match_elements_response_body(records)
        match_records(records, :match_element_response_body)
      end

      def match_response_response_body(record)
        expect(record).to be_key("data")
        expect(record.dig("data")).to be_key("id")
      end

      def match_records(records, match_serializer)
        record = records["data"].first
        expect(records).to be_key("data")
        send(match_serializer, { "data" => record })
      end

      # rubocop:disable Metrics/MethodLength
      def match_form_metrics_response_body(record)
        expect(record).to be_key("data")
        expect(record.dig("data", "attributes")).to be_key("total_responses")
        expect(record.dig("data", "attributes")).to be_key("element_metrics")

        expect(record.dig("data", "attributes", "element_metrics"))
          .to be_key("answer_text_metrics")
        answer_text_metrics = record.dig("data", "attributes",
                                         "element_metrics", "answer_text_metrics")[0]
        expect(answer_text_metrics).to be_key("id")
        expect(answer_text_metrics).to be_key("title")
        expect(answer_text_metrics).to be_key("title_edited_with_answers")
        expect(answer_text_metrics).to be_key("position")
        expect(answer_text_metrics).to be_key("required")
        expect(answer_text_metrics).to be_key("type_of_element")
        expect(answer_text_metrics).to be_key("no_of_answers")
        expect(answer_text_metrics).to be_key("answer_detail")

        expect(record.dig("data", "attributes", "element_metrics"))
          .to be_key("answer_choice_metrics")
        answer_choice_metrics = record.dig("data", "attributes",
                                           "element_metrics", "answer_choice_metrics")[0]
        expect(answer_choice_metrics).to be_key("element_id")
        expect(answer_choice_metrics).to be_key("title")
        expect(answer_choice_metrics).to be_key("title_edited_with_answers")
        expect(answer_choice_metrics).to be_key("position")
        expect(answer_choice_metrics).to be_key("required")
        expect(answer_choice_metrics).to be_key("element_type")
        expect(answer_choice_metrics).to be_key("total_responses")
        expect(answer_choice_metrics).to be_key("total_non_responses")
        expect(answer_choice_metrics).to be_key("element_metrics")

        element_metrics = answer_choice_metrics.dig("element_metrics")[0]
        expect(element_metrics).to be_key("value")
        expect(element_metrics).to be_key("option")
        expect(element_metrics).to be_key("no_of_answers")
      end
      # rubocop:enable Metrics/MethodLength

      def match_element_choice_response_body(record)
        expect(record).to include_json(
          data: { id: [],
                  type: [],
                  attributes: { title: [] } }
        )
      end

      def match_element_choices_response_body(records)
        match_records(records, :match_element_choice_response_body)
      end

      def match_email_recipient_response_body(record)
        expect(record).to be_key("data")
        expect(record.dig("data", "attributes")).to be_key("email")
        expect(record.dig("data", "relationships", "form")).to be_key("links")
      end

      def match_email_recipients_response_body(records)
        match_records(records, :match_email_recipient_response_body)
      end
    end

    # rubocop:enable Metrics/AbcSize, Metrics/ModuleLength
  end
end

RSpec.configure do |config|
  config.include Api::V1::RequestHelpers, type: :request
  config.include Api::V1::ResponseFormat, type: :request
end
