# frozen_string_literal: true

module Api
  module V1
    module Forms
      class DuplicatesController < ::Api::V1::ApiController
        def create
          duplicated_form = form.duplicate!

          if duplicated_form.persisted?
            render json: serialize_form(duplicated_form), status: :created
          else
            render json: { errors: duplicated_form.errors }, status: :unprocessable_entity
          end
        end

        private

        def form
          @form ||= organization_auth_scope(Form).includes(
            elements: :element_choices
          ).find_by!(id: params[:form_id])
        end
      end
    end
  end
end
