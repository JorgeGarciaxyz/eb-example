# frozen_string_literal: true

require "rails_helper"

RSpec.describe Builders::FormResponse::AnswerChoiceBuilder do
  subject do
    described_class.new(answer_data, response).call
  end

  let(:form) { create(:form) }
  let(:choice_type_element) { create(:element, :single_choice, form: form) }
  let(:choice_for_element) { create(:element_choice, element: choice_type_element) }
  let(:response) { create(:response, form: form) }
  let(:answer_data) do
    {
      element_id: choice_type_element&.id,
      element_choice_id: choice_for_element&.id
    }
  end

  describe "call" do
    context "when receives valid information" do
      it "builds a valid AnswerChoice element" do
        expect(subject.class).to eql(::AnswerChoice)
        expect(subject.valid?).to be true
      end
    end

    context "when response is not valid" do
      let(:response) { nil }

      it "builds an invalid AnswerChoice element" do
        expect(subject.valid?).to be false
      end
    end

    context "when choice_id is not valid" do
      let(:choice_for_element) { nil }

      it "builds an invalid AnswerChoice element" do
        expect(subject.valid?).to be false
      end
    end
  end
end
