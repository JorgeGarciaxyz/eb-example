# frozen_string_literal: true

module Builders
  module FormResponse
    # Builder interface
    module Builder
      extend ActiveSupport::Concern

      def call
        raise "not implemented"
      end

      included do
        private

        def build_answer
          raise "not implemented"
        end

        def values
          raise "not implemented"
        end
      end
    end
  end
end
