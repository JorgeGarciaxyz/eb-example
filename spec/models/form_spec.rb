# frozen_string_literal: true

require "rails_helper"

RSpec.describe Form, type: :model do
  subject { create(:form) }

  it { expect(subject).to be_valid }

  describe "associations" do
    it { is_expected.to have_many(:elements) }
    it { is_expected.to have_many(:responses) }
    it { is_expected.to have_many(:email_recipients) }
  end

  describe "validations" do
    it { is_expected.to validate_presence_of(:name) }
    it { is_expected.to validate_presence_of(:secondary_organization_id) }
    it { is_expected.to validate_presence_of(:status) }
    it { is_expected.to allow_value(true).for(:school_website_visibility) }
    it { is_expected.to allow_value(false).for(:school_website_visibility) }
    it { is_expected.to validate_length_of(:name).is_at_least(3).is_at_most(100) }

    it do
      expect(subject).to validate_uniqueness_of(:unique_id)
    end

    it do
      expect(subject).to validate_uniqueness_of(:name).ignoring_case_sensitivity
        .scoped_to(:secondary_organization_id)
    end

    it { is_expected.to validate_inclusion_of(:status).in_array(Form::FORM_STATUS_ENUM) }

    context "when form is active?" do
      subject { create(:form, :active) }

      it "doesnt allow past close dates" do
        expect(subject).not_to allow_value(Time.zone.now - 1.day).for(:close_date)
      end

      it "allows future close dates" do
        expect(subject).to allow_value(Time.zone.now + 1.day).for(:close_date)
      end

      it "allows nil on close_date" do
        expect(subject).to allow_value(nil).for(:close_date)
      end
    end

    context "when form is not active?" do
      subject { create(:form, :draft) }

      it "allows past close dates" do
        expect(subject).to allow_value(Time.zone.now - 1.day).for(:close_date)
      end
    end
  end

  describe "custom validations" do
    context "when settings is SCHOOL_WEBSITE_VISIBILITY" do
      context "with valid setting" do
        subject { build(:form_with_all_valid_settings) }

        it { expect(subject).to be_valid }
      end

      context "with invalid setting" do
        subject { build(:form_invalid_settings) }

        it { expect(subject).not_to be_valid }

        # rubocop:disable RSpec/ExampleLength
        it "return the expected error messages" do
          expected_error = I18n.t(
            "activerecord.errors.models.form.attributes.settings.format",
            keys: Form::FORM_ALLOWED_SETTINGS
          )

          subject.valid?

          expect(subject.errors.messages.dig(:settings)[0]).to eq(expected_error)
        end
        # rubocop:enable RSpec/ExampleLength
      end
    end

    context "with name uniqueness" do
      context "when status is closed" do
        let(:pre_existing_form) { create :form, :closed }

        # rubocop:disable RSpec/ExampleLength
        it "allows duplicate names" do
          new_form = build(
            :form,
            name: pre_existing_form.name,
            secondary_organization_id: pre_existing_form.secondary_organization_id
          )

          expect(new_form).to be_valid
        end
        # rubocop:enable RSpec/ExampleLength
      end

      context "when status is archived" do
        let(:pre_existing_form) { create :form, :archived }

        # rubocop:disable RSpec/ExampleLength
        it "allows duplicate names" do
          new_form = build(
            :form,
            name: pre_existing_form.name,
            secondary_organization_id: pre_existing_form.secondary_organization_id
          )

          expect(new_form).to be_valid
        end
        # rubocop:enable RSpec/ExampleLength
      end

      context "when status is NOT closed nor archived" do
        let(:pre_existing_form) { create :form }

        # rubocop:disable RSpec/ExampleLength
        it "doesn't allow duplicate names" do
          new_form = build(
            :form,
            name: pre_existing_form.name,
            secondary_organization_id: pre_existing_form.secondary_organization_id
          )

          expect(new_form).not_to be_valid
        end
        # rubocop:enable RSpec/ExampleLength
      end
    end
  end

  describe ".visible_on_school_website_form" do
    let!(:visible_on_school_website_form) do
      create(:form, :visible_on_school_website)
    end

    before do
      create(:form, :archived, :false_school_website_visibility)
      create(:form, :closed, :false_school_website_visibility)
    end

    it "return only schools who match the scope" do
      expect(described_class.visible_on_school_website).to match_array(
        [visible_on_school_website_form]
      )
    end
  end

  describe "with direct link visibility" do
    let(:form_with_direct_link) do
      create(:form, :direct_link_visibility_true)
    end

    before do
      form_with_direct_link
      create(:form, :direct_link_visibility_false)
    end

    it "retuns only forms with direct link visibility" do
      expect(described_class.with_direct_link_visibility).to match_array(
        [form_with_direct_link]
      )
    end
  end

  describe "without direct link visibility" do
    let(:form_without_direct_link) do
      create(:form, :direct_link_visibility_false)
    end

    before do
      form_without_direct_link
      create(:form, :direct_link_visibility_true)
    end

    it "retuns only forms without direct link visibility" do
      expect(described_class.without_direct_link_visibility).to match_array(
        [form_without_direct_link]
      )
    end
  end

  describe "direct link visibility based on auth" do
    let(:form_without_direct_link) do
      create(:form, :direct_link_visibility_false)
    end

    before do
      form_without_direct_link
      create(:form, :direct_link_visibility_true)
    end

    context "with Current jwt set" do
      before do
        Current.jwt = {}
      end

      it "returns all forms" do
        expect(described_class.direct_link_visibility_based_on_auth.size).to eq(2)
      end
    end

    context "without Current jwt set" do
      it "returns forms without direct link visibility" do
        expect(described_class.direct_link_visibility_based_on_auth).to match_array(
          [form_without_direct_link]
        )
      end
    end
  end

  describe "#close!" do
    subject { create(:form, :active) }

    it "changes the status to SURVEY_STATUS_CLOSED" do
      expect { subject.close! }.to change(
        subject, :status
      ).from("SURVEY_STATUS_ACTIVE").to("SURVEY_STATUS_CLOSED")
    end
  end

  describe "callbacks" do
    describe "#schedule_closure" do
      subject { build(:form, close_date: Date.tomorrow.noon) }

      context "when close_date is updated to be executed on the close date" do
        it "enqueues a CloseFormJob" do
          expect { subject.save }.to have_enqueued_job.with(
            subject.id
          ).at(Date.tomorrow.noon)
        end
      end
    end

    describe "before_destroy" do
      context "with draft status and has no responses" do
        let!(:subject) { create(:form, :draft) }

        it "is deleted" do
          expect { subject.destroy }.to change(described_class, :count).by(-1)
        end
      end

      context "with draft status and has responses" do
        let!(:subject) { create(:form, :active) }

        before do
          create(:response, form: subject)
          subject.update(status: "SURVEY_STATUS_DRAFT")
        end

        it "is not deleted" do
          expect { subject.destroy }.not_to change(described_class, :count)
        end
      end

      context "with not draft status" do
        let!(:subject) { create(:form, :active) }

        it "is not deleted" do
          expect { subject.destroy }.not_to change(described_class, :count)
        end
      end
    end
  end
end
