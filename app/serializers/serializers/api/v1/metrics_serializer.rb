# frozen_string_literal: true

module Api
  module V1
    # Form Metrics Serializer
    class MetricsSerializer
      include JSONAPI::Serializer

      set_id "null" # This is not a real resource, doesn't have an id

      attributes :total_responses, :element_metrics
    end
  end
end
