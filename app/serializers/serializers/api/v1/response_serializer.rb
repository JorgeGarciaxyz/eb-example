# frozen_string_literal: true

module Api
  module V1
    # ResponseSerializer
    class ResponseSerializer
      include JSONAPI::Serializer
    end
  end
end
