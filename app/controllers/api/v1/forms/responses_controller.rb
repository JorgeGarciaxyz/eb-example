# frozen_string_literal: true

module Api
  module V1
    module Forms
      # Form's responses
      class ResponsesController < ::Api::V1::ApiController
        include JSONAPI::Deserialization

        skip_before_action :authenticate_request!, only: %i[create]
        before_action :form_is_active?, only: %i[create]

        def create
          service = Builders::FormResponse::Director.new(
            form, response_params.dig(:answers)
          ).call

          if service.success?
            render json: serialize_response(service.result)
          else
            render json: { errors: service.errors }, status: :unprocessable_entity
          end
        end

        private

        def response_params
          jsonapi_deserialize(
            params, only: %i[answers form]
          ).with_indifferent_access
        end

        def form
          @form ||= Form.find_by(unique_id: params[:form_unique_id])
        end

        def form_is_active?
          return if form.active?

          form.errors.add(:status, :not_active)

          render json: { errors: form.errors }, status: :unprocessable_entity
        end
      end
    end
  end
end
