# frozen_string_literal: true

module Builders
  module FormResponse
    # AnswerTextBuilder
    class AnswerTextBuilder
      include ::Builders::FormResponse::Builder

      def initialize(answer_params, response)
        @answer_params = answer_params
        @response = response
      end

      def call
        build_answer
      end

      private

      attr_reader :answer_params, :response

      def build_answer
        ::AnswerText.new(values)
      end

      def values
        {
          element_id: answer_params.dig(:element_id),
          text_response: answer_params.dig(:answer_text),
          response: response
        }
      end
    end
  end
end
