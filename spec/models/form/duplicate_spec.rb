# frozen_string_literal: true

require "rails_helper"

RSpec.describe Form::Duplicate do
  subject { described_class.new(form) }

  let(:form) { create(:form) }
  let(:elements) do
    [:short_answer, :single_choice].map do |element_type|
      create(:element, element_type, :required, form: form)
    end
  end
  let!(:element_choices) do
    create_list(:element_choice, 3, element: elements.last)
  end

  describe "#duplicate_form_and_children" do
    let(:duplicated_form) { subject.tap(&:duplicate_form_and_children).duplicated_form }

    it "duplicate form object with the expected attributes" do
      expected_attributes = form.attributes.slice("secondary_organization_id")

      expect(
        duplicated_form.attributes.slice("secondary_organization_id")
      ).to match(expected_attributes)
    end

    context "when the form name is longer than 100 chars" do
      let(:form) { create(:form, name: "F" * 99) }

      it "partially matches the form name" do
        expect(duplicated_form.name).to eq("Copy of #{"F" * 89}...")
      end
    end

    context "when the form name is not longer than 100 chars" do
      it "partially matches the form name" do
        expect(duplicated_form.name).to eq("Copy of #{form.name}")
      end
    end

    # rubocop:disable RSpec/ExampleLength
    it "duplicate elements objects with the expected attributes" do
      expected_attributes = slice_attributes_from_collection(
        elements, Form::Duplicate::ELEMENT_DUPLICATED_VALUES
      )

      expect(
        slice_attributes_from_collection(
          duplicated_form.elements, Form::Duplicate::ELEMENT_DUPLICATED_VALUES
        )
      ).to match_array(expected_attributes)
    end

    it "duplicate element_choices objects with the expected attributes" do
      expected_attributes = slice_attributes_from_collection(
        element_choices, Form::Duplicate::ELEMENT_CHOICE_DUPLICATED_VALUES
      )

      expect(
        # we cant get the element_choices from the form, event if its using a has_many
        # through; this won't work if we have non-persisted objects
        slice_attributes_from_collection(
          element_choices_from_element_with_choices(duplicated_form.elements),
          Form::Duplicate::ELEMENT_CHOICE_DUPLICATED_VALUES
        )
      ).to match_array(expected_attributes)
    end
    # rubocop:enable RSpec/ExampleLength
  end

  def slice_attributes_from_collection(collection, attributes)
    collection.map do |obj|
      obj.slice(*attributes)
    end
  end

  def element_choices_from_element_with_choices(elements)
    elements.each do |element|
      return element.element_choices if element.element_choices.present?
    end
  end
end
