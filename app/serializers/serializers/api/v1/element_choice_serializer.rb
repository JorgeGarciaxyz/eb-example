# frozen_string_literal: true

module Api
  module V1
    # ElementSerializer
    class ElementChoiceSerializer
      include JSONAPI::Serializer

      attributes :title
    end
  end
end
