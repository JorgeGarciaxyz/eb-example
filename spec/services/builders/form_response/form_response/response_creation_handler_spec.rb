# frozen_string_literal: true

require "rails_helper"
require "./spec/support/shared_contexts/form_response_builder"

RSpec.describe Builders::FormResponse::ResponseCreationHandler do
  subject do
    described_class.new(
      form, element_ids_from_params, answers_params
    ).tap(&:call)
  end

  include_context "with form response builder"

  let(:form) { create(:form) }
  let(:element_ids_from_params) { elements.map(&:id) }

  describe "call" do
    context "when answer text" do
      let(:answer_text_elements) { create_list(:element, 2, :short_answer, form: form) }
      let(:elements) { answer_text_elements }
      let(:answers_params) { generate_answers_params(elements) }

      it "build the answer objects" do
        expect(subject.send(:answer_objects).dig(:answer_text).count).to eq(
          answer_text_elements.size
        )
      end
    end

    context "when multiple choice" do
      let(:element_type) { create(:element_type, :multiple_choice) }
      let(:elements) { create_list(:element, 1, element_type: element_type, form: form) }
      let(:answers_params) { generate_multiple_choice_params(elements.first, 3) }

      it "builds the answer objects for multiple choice" do
        expect(subject.send(:answer_objects).dig(:answer_choice).count).to eq(3)
      end
    end
  end
end
