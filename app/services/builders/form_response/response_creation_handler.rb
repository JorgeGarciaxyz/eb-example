# frozen_string_literal: true

module Builders
  module FormResponse
    # ResponseCreationHandler
    class ResponseCreationHandler
      def initialize(form, element_ids_from_params, answers_params)
        @answer_objects = { answer_text: [], answer_choice: [] }
        @elements_with_type_of_answer = elements_with_type_of_answer(
          element_ids_from_params
        )
        @response = response(form)
        @answers_params = answers_params
      end

      def call
        build_answer_objects(answers_params)

        ::Builders::FormResponse::ResponseCreator.call(response, answer_objects)
      end

      private

      attr_accessor :answer_objects
      attr_reader :form, :answers_params

      def elements_with_type_of_answer(element_ids_from_params = [])
        @elements_with_type_of_answer ||= ::Element.where(
          id: element_ids_from_params
        ).joins(:element_type).select("elements.id, type_of_answer")
      end

      def response(form = nil)
        @response ||= ::Response.new(form: form)
      end

      def build_answer_objects(answers_params)
        answers_params.each do |answer|
          type_of_answer = elements_with_type_of_answer.find do |element|
            element.id == answer.dig(:element_id).to_i
          end.type_of_answer

          build_answer_based_on_type(answer, type_of_answer)
        end
      end

      def build_answer_based_on_type(answer, type_of_answer)
        type = type_of_answer.downcase
        builder = "::Builders::FormResponse::#{type.camelize}Builder".constantize

        answer_objects[type.to_sym] << builder.new(answer, response).call
      end
    end
  end
end
