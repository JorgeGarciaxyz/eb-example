# frozen_string_literal: true

module Builders
  module FormResponse
    # Support module to format errors standardized
    module ErrorsStructure
      extend ActiveSupport::Concern

      included do
        private

        def add_formated_errors(attributes: {}, errors: [])
          self.errors << { attributes: attributes, errors: [*errors] }
          handle_errors_flow
        end
      end
    end
  end
end
