# frozen_string_literal: true

require "rails_helper"
require "support/api/v1/request_helpers"
require "support/auth_support"
require "./spec/support/shared_contexts/duplicable_form_context"

RSpec.describe Api::V1::Forms::DuplicatesController, type: :request do
  describe "POST create" do
    include_context "with duplicable form"

    before do
      authorized_request(:post, "/api/v1/forms/#{form.id}/duplicates")
    end

    context "when request is succesful" do
      it "success" do
        expect(response).to have_http_status(:created)
        match_form_response_body(json_response)
      end
    end
  end
end
