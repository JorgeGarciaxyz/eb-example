# frozen_string_literal: true

module Api
  module V1
    module Authentication
      extend ActiveSupport::Concern
      included do
        before_action :authenticate_request!
      end

      private

      def authenticate_request!
        token = decode_token
        return if Current.jwt

        render json: { errors: token.errors }, status: :unauthorized
      end

      def decode_token
        decode = ::JwtWrapper::Decoder.call(bearer_token)

        Current.jwt = decode.result if decode.success?
        decode
      end

      # Uses the standard way to get the token from the headers
      # https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Authorization
      def bearer_token
        @bearer_token ||= request.headers["Authorization"]&.split(" ")&.second
      end

      def optional_auth_secondary_org_id
        Current.jwt&.dig(:secondary_organization_id) || params[:secondary_organization_id]
      end
    end
  end
end
