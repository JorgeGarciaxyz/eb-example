# frozen_string_literal: true

require "rails_helper"
require "support/auth_support"

RSpec.describe FormCsvReportExportChannel, type: :channel do
  describe "subscription" do
    let(:fake_token) { AuthSupport.decoded_test_token }
    let(:form) do
      create(:form, secondary_organization_id: fake_token.secondary_organization_id)
    end

    before do
      stub_connection(current_jwt: fake_token)
    end

    context "when is succesful" do
      let(:params) { { unique_id: "meep", form_id: form.id } }

      it "subscribes succesfully" do
        subscribe(params)
        expect(subscription).to be_confirmed
      end

      it "enqueue the FormCsvReportExportJob" do
        allow(FormCsvReportExportJob).to receive(:perform_later)

        subscribe(params)

        expect(FormCsvReportExportJob).to have_received(:perform_later)
      end
    end

    context "when is not succesful" do
      before do
        subscribe(params)
      end

      context "with missing params" do
        let(:params) { {} }

        it do
          expect(subscription).to be_rejected
        end
      end

      context "when trying to access other org's form" do
        let(:params) do
          form = create(
            :form, secondary_organization_id: fake_token.secondary_organization_id + 1
          )

          { unique_id: "meep", form_id: form.id }
        end

        it do
          expect(subscription).to be_rejected
        end
      end
    end
  end
end
