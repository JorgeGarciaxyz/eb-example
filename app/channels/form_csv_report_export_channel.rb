# frozen_string_literal: true

class FormCsvReportExportChannel < ApplicationCable::Channel
  def self.channel_name(unique_id)
    "form_csv_report_export_channel_#{unique_id}"
  end

  def subscribed
    return reject unless params[:unique_id].present? && form

    stream_from(channel_name(params[:unique_id]))

    FormCsvReportExportJob.perform_later(form, params[:unique_id])
  end

  def unsubscribed; end

  private

  def form
    @form ||= Form.find_by(
      id: params[:form_id],
      secondary_organization_id: current_jwt.secondary_organization_id
    )
  end
end
