# frozen_string_literal: true

module Api
  module V1
    # FormSerializer
    class FormSerializer
      include JSONAPI::Serializer

      attributes :secondary_organization_id, :name, :status, :close_date, :url,
                 :settings, :created_at, :unique_id

      has_many :elements, lazy_load_data: true, links: {
        related: lambda { |object|
          Rails.application.routes.url_helpers.api_v1_form_elements_path(
            form_id: object.id
          )
        }
      }

      has_many :metrics, lazy_load_data: true, links: {
        related: lambda { |object|
          Rails.application.routes.url_helpers.api_v1_form_metrics_path(
            form_id: object.id
          )
        }
      }

      has_many :responses, lazy_load_data: true, meta: lambda { |object|
        { count: object.response_count }
      }, if: ->(_, params) { params.dig(:meta, :response_count) }
    end
  end
end
