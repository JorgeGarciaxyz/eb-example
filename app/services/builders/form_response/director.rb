# frozen_string_literal: true

module Builders
  module FormResponse
    # Director of Builder, implementation of Builder Design Pattern
    # As the result of the #call it is expected to be used on the Controller, each
    # returned value must be an instance using ApplicationService
    class Director
      # answer_params expected format (it is defined on the openapi):
      # answers: [{ element_id: 1, answer_text: "yes" }]
      def initialize(form, answers_params)
        @form = form
        @answers_params = answers_params
      end

      def call
        result = ::Builders::FormResponse::ParamsValidator.call(
          form, answers_params, element_ids_from_params
        )
        return result if result.errors.present?

        ::Builders::FormResponse::ResponseCreationHandler.new(
          form, element_ids_from_params, answers_params
        ).call
      end

      private

      attr_accessor :answers_params
      attr_reader :form

      def element_ids_from_params
        @element_ids_from_params ||= answers_params.map { |a| a.dig(:element_id).to_i }
      end
    end
  end
end
