# frozen_string_literal: true

module Api
  module V1
    # FormsController
    class FormsController < ::Api::V1::ApiController
      include JSONAPI::Deserialization
      include Api::Pagination
      include ::Api::V1::Localeable

      rescue_from Errors::Form::QueryMinimumLengthError, with: :bad_request

      skip_before_action :authenticate_request!, only: %i[show index]
      before_action :decode_token, only: %i[show index]

      def show
        form = Form.direct_link_visibility_based_on_auth.find_by!(
          id: params[:id],
          secondary_organization_id: optional_auth_secondary_org_id
        )

        render json: serialize_form(form.decorate, response_count_params)
      end

      def create
        form = Form.new(create_params)

        if form.save
          render json: serialize_form(form)
        else
          render json: { errors: form.errors }, status: :unprocessable_entity
        end
      end

      def update
        if form.update(update_params)
          render json: serialize_form(form)
        else
          render json: { errors: form.errors }, status: :bad_request
        end
      end

      def index
        forms = Api::V1::FormFetcherService.new(
          params,
          secondary_organization_id: optional_auth_secondary_org_id,
          initial_scope: :direct_link_visibility_based_on_auth
        ).call

        forms, links = paginate(forms.decorate)

        options = { links: links }.merge(response_count_params)
        render json: serialize_form(forms, options)
      end

      def destroy
        if form.destroy
          render status: :ok
        else
          render json: { errors: form.errors }, status: :unprocessable_entity
        end
      end

      private

      def form
        @form ||= Form.find_by!(
          id: params[:id], secondary_organization_id: secondary_organization_id
        )
      end

      def form_params
        jsonapi_deserialize(
          params, only: %i[name close_date status settings]
        )
      end

      def create_params
        form_params.merge(secondary_organization_id: secondary_organization_id)
      end

      def update_params
        form_params
      end

      def response_count_params
        return {} if params[:meta].blank?

        { params: { meta: { response_count: params[:meta][:response_count] } } }
      end
    end
  end
end
