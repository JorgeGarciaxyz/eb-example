# frozen_string_literal: true

require "rails_helper"
require "./spec/support/shared_contexts/form_response_builder"

RSpec.describe Builders::FormResponse::ParamsValidator do
  subject do
    described_class.call(form, answers_params, element_ids_from_params)
  end

  include_context "with form response builder"

  let(:form) { create(:form) }
  let(:elements) do
    create_list(
      :element,
      2,
      :short_answer,
      form: form,
      config: { required_answer: true }
    )
  end
  let(:element_ids_from_params) { answers_params.map { |a| a.dig(:element_id) } }
  let(:answers_params) { generate_answers_params(elements) }

  describe "call" do
    context "when params are valid" do
      it do
        expect(subject.success?).to be(true)
      end
    end

    context "when required elements params are missing" do
      let(:answers_params) do
        generate_answers_params(elements.take(1))
      end

      # rubocop:disable RSpec/ExampleLength
      it "returns expected errors" do
        expected_errors = [
          expected_errors_object(
            missing_required_elements_errors(missing_required_elements(elements))
          )
        ]

        expect(subject.success?).to be(false)
        expect(subject.errors).to match_array(expected_errors)
      end
      # rubocop:enable RSpec/ExampleLength
    end

    context "when elements do not belong to form" do
      let(:form2) { create(:form) }
      let(:elements) { create_list(:element, 2, form: form2) }

      it "returns expected errors" do
        expected_errors = [
          expected_errors_object(elements_not_belong_to_form_errors(elements.map(&:id)))
        ]

        expect(subject.success?).to be(false)
        expect(subject.errors).to match_array(expected_errors)
      end
    end

    context "when params have all kind of errors" do
      let(:form2) { create(:form) }
      let(:elements) do
        create_list(
          :element, 2, :short_answer, form: form2, config: { required_answer: true }
        )
      end

      before do
        # create forms required elements
        create_list(:element, 2, form: form, config: { required_answer: true })
      end

      # rubocop:disable RSpec/ExampleLength
      it "return expected errors" do
        expected_errors = [
          expected_errors_object(missing_required_elements_errors(form.elements.ids)),
          expected_errors_object(elements_not_belong_to_form_errors(elements.map(&:id)))
        ]

        expect(subject.success?).to be(false)
        expect(subject.errors).to match_array(expected_errors)
      end
      # rubocop:enable RSpec/ExampleLength
    end
  end

  def expected_errors_object(errors)
    { attributes: {}, errors: [errors] }
  end

  def missing_required_elements_errors(elements)
    I18n.t(
      "#{builders_errors_context}.missing_required_elements",
      elements: elements
    )
  end

  def elements_not_belong_to_form_errors(elements)
    I18n.t(
      "#{builders_errors_context}.elements_not_belong_to_form",
      elements: elements
    )
  end

  def builders_errors_context
    "services.builders.forms_response."
  end

  def missing_required_elements(elements)
    elements_ids = elements.map(&:id)
    elements_ids - element_ids_from_params
  end
end
