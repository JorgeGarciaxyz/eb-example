# frozen_string_literal: true

# == Schema Information
#
# Table name: forms
#
#  id                        :bigint           not null, primary key
#  close_date                :datetime
#  name                      :text             not null
#  settings                  :jsonb            not null
#  status                    :enum             default("SURVEY_STATUS_DRAFT"), not null
#  url                       :text
#  created_at                :datetime         not null
#  updated_at                :datetime         not null
#  close_form_job_id         :string
#  secondary_organization_id :text             not null
#  unique_id                 :string           not null
#
# Indexes
#
#  index_forms_on_secondary_organization_id  (secondary_organization_id)
#

require "securerandom"

class Form < ApplicationRecord
  include Form::Duplicable
  include Form::FilterableByScope
  include Form::ElasticSearchable
  include Metricable
  include VisibilityToggleable
  include CloseDateSetter

  # The enum is handled by postgres, there is no need to define the enum on the model
  FORM_STATUS_ENUM = %w[
    SURVEY_STATUS_DRAFT SURVEY_STATUS_SCHEDULED SURVEY_STATUS_ACTIVE
    SURVEY_STATUS_ARCHIVED SURVEY_STATUS_CLOSED
  ].freeze

  FORM_ALLOWED_SETTINGS = Schemas::Form::SettingsSchema::SCHEMA.dig(:properties)
    .keys.freeze

  has_many :elements, -> { order(position: :asc) }, dependent: :destroy, inverse_of: :form
  has_many :element_choices, through: :elements
#  has_many :responses, dependent: :destroy, inverse_of: :form
  has_one :response
  has_many :email_recipients, dependent: :destroy, inverse_of: :form

  store_accessor :settings,
                 :school_website_visibility,
                 :school_mobile_app_visibility,
                 :send_email_notifications

  validates :close_date, inclusion: { in: Time.zone.now.. },
                         allow_blank: true, if: :active?
  validates :name, presence: true, uniqueness: {
    scope: :secondary_organization_id,
    case_sensitive: false,
    conditions: -> { where.not(status: %w[SURVEY_STATUS_ARCHIVED SURVEY_STATUS_CLOSED]) }
  }, length: { minimum: 3, maximum: 100 }
  validates :secondary_organization_id, presence: true
  validates :settings, json: { schema: -> { Schemas::Form::SettingsSchema::SCHEMA },
                               message: I18n.t(
                                 "activerecord.errors.models"\
                                 ".form.attributes.settings.format",
                                 keys: FORM_ALLOWED_SETTINGS
                               ) }
  validates :status, presence: true, inclusion: { in: FORM_STATUS_ENUM }
  # We are using UUID as the preferred string generator.
  # This uniqueness validation is in case something fails,
  # but the probability is close to zero.
  validates :unique_id, uniqueness: true

  before_create :set_unique_id
  before_destroy :can_destroy_form, prepend: true
  before_save :schedule_closure, if: :will_save_change_to_close_date?
  before_save :update_settings_visibility_toggles, if: :will_save_change_to_settings?

  after_update :update_close_date, if: :saved_change_to_status?

  scope :active, -> { where(status: "SURVEY_STATUS_ACTIVE") }
  scope :archived, -> { where(status: "SURVEY_STATUS_ARCHIVED") }
  scope :closed, -> { where(status: "SURVEY_STATUS_CLOSED") }
  scope :drafted, -> { where(status: "SURVEY_STATUS_DRAFT") }
  scope :scheduled, -> { where(status: "SURVEY_STATUS_SCHEDULED") }

  scope :name_search, lambda { |query|
    raise Errors::Form::QueryMinimumLengthError if query.length < 3

    where("name ILIKE(?)", "%#{query}%")
  }

  scope :visible_on_school_mobile_app, lambda {
    where("settings ->> 'school_mobile_app_visibility' = 'true'").active
  }

  scope :visible_on_school_website, lambda {
    where("settings ->> 'school_website_visibility' = 'true'").active
  }

  scope :with_direct_link_visibility, lambda {
    where("settings ->> 'direct_link_visibility' = 'true'")
  }

  scope :without_direct_link_visibility, lambda {
    where("settings ->> 'direct_link_visibility' = 'false'")
  }

  scope :direct_link_visibility_based_on_auth, lambda {
    without_direct_link_visibility unless Current.jwt
  }

  scope :with_response_count, lambda {
    left_joins(:responses)
      .select("forms.*, count(responses.id) as query_response_count")
      .group("forms.id")
  }

  def response_count
    respond_to?(:query_response_count) ? query_response_count : responses.count
  end

  def close!
    update(status: "SURVEY_STATUS_CLOSED")
  end

  def active?
    status == "SURVEY_STATUS_ACTIVE"
  end

  private

  def generate_unique_id
    SecureRandom.uuid
  end

  def set_unique_id
    self.unique_id = generate_unique_id
  end

  def schedule_closure
    self.close_form_job_id = CloseFormJob
      .set(wait_until: close_date)
      .perform_later(id)
      .job_id
  end

  def can_destroy_form
    return if status == "SURVEY_STATUS_DRAFT" && responses.empty?

    errors.add(
      :status, :cannot_delete_non_draft_with_responses
    )
    throw(:abort)
  end
end
