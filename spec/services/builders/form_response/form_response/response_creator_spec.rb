# frozen_string_literal: true

require "rails_helper"

# rubocop: disable RSpec/MultipleMemoizedHelpers

RSpec.describe Builders::FormResponse::ResponseCreator do
  subject do
    described_class.call(response, answer_objects)
  end

  let(:form) { create(:form) }
  let(:text_type_elements) do
    create_list(
      :element, 2, :short_answer, form: form, config: { character_no_limit: 200 }
    )
  end
  let(:choice_type_elements) { create_list(:element, 2, :single_choice, form: form) }
  let(:response) { build(:response, form: form) }

  let(:text_answers) do
    text_type_elements.map do |element|
      build(:answer_text, element: element, response: response)
    end
  end

  let(:choice_answers) do
    choice_type_elements.map do |element|
      build(:answer_choice,
            element_choice_id: element.element_choices.first.id,
            response: response,
            element: element)
    end
  end

  let(:answer_objects) do
    {
      answer_choice: choice_answers,
      answer_text: text_answers
    }
  end

  before do
    choice_type_elements.each do |element|
      create(:element_choice, element: element)
    end
  end

  describe "call" do
    context "when it receives valid information" do
      it "returns true on success?" do
        expect(subject.success?).to be true
      end

      it "creates new responses with associated answers" do
        assert_created_objects(
          new_responses: 1,
          new_answer_texts: answer_objects[:answer_text].size,
          new_answer_choices: answer_objects[:answer_choice].size
        )
      end
    end

    context "when response is not valid" do
      let(:response) { build(:response, form: nil) }

      it "returns false on success?" do
        expect(subject.success?).to be false
      end

      it "does not create a response nor associated answers" do
        assert_created_objects(
          new_responses: 0,
          new_answer_texts: 0,
          new_answer_choices: 0
        )
      end

      it "returns the expected errors" do
        expect(subject.errors.size).to eq(1)
        expect(subject.errors.first).to be_key(:attributes)
        expect(subject.errors.first).to be_key(:errors)
      end
    end

    context "when any of the answers is not valid" do
      before do
        answer_objects[:answer_text].take(1).tap { |a| a.last.text_response = nil }
        answer_objects[:answer_choice].tap { |a| a.last.element_choice_id = nil }
        update_answer_text_to_exceed_character_limit(answer_objects[:answer_text].last)
      end

      it "returns false on success?" do
        expect(subject.success?).to be false
      end

      it "does not create a response nor associated answers" do
        assert_created_objects(
          new_responses: 0,
          new_answer_texts: 0,
          new_answer_choices: 0
        )
      end

      it "returns the expected errors" do
        expect(subject.errors.size).to eq(3)
        expect(subject.errors.first).to be_key(:attributes)
        expect(subject.errors.first).to be_key(:errors)
      end

      context "when form has email recipients" do
        let(:form) { create(:form, :send_email_notifications) }

        before { create(:email_recipient, form: form) }

        it "doesn't send response's email notification" do
          expect do
            subject
          end.not_to have_enqueued_mail(FormResponseMailer, :response_submission_notice)
        end
      end
    end
  end

  def assert_created_objects(new_responses:, new_answer_texts:, new_answer_choices:)
    expect { subject }
      .to change(Response, :count).by(new_responses)
      .and change(AnswerText, :count).by(new_answer_texts)
      .and change(AnswerChoice, :count).by(new_answer_choices)
  end

  def update_answer_text_to_exceed_character_limit(answer_text)
    character_no_limit = answer_text.element.config.dig("character_no_limit")
    answer_text.text_response = Faker::Name.initials(number: character_no_limit + 1)
  end
end

# rubocop: enable RSpec/MultipleMemoizedHelpers
