# frozen_string_literal: true

module Api
  module V1
    class EmailRecipientSerializer
      include JSONAPI::Serializer

      attributes :email

      belongs_to :form, lazy_load_data: true, links: {
        related: lambda { |object|
          Rails.application.routes.url_helpers.api_v1_form_path(
            id: object.form_id
          )
        }
      }
    end
  end
end
