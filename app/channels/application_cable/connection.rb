# frozen_string_literal: true

module ApplicationCable
  class Connection < ActionCable::Connection::Base
    identified_by :current_jwt

    def connect
      authenticate_request
    end

    private

    def bearer_token
      @bearer_token ||= request.params["bearer_token"]
    end

    def authenticate_request
      decoded_token = JwtWrapper::Decoder.call(bearer_token)

      return reject_unauthorized_connection unless decoded_token.success?

      self.current_jwt = decoded_token.result
    end
  end
end
