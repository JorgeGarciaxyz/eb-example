# frozen_string_literal: true

module Api
  module V1
    # ApiController
    class ApiController < ApplicationController
      include ::Api::V1::Authentication
      include ::Api::V1::SerializableResources
      include ::Api::V1::TimezoneLocalizable

      rescue_from ActiveRecord::RecordNotFound, with: :record_not_found

      private

      def secondary_organization_id
        Current.jwt[:secondary_organization_id]
      end

      def organization_auth_scope(klass)
        if klass == Form
          klass.where(secondary_organization_id: secondary_organization_id)
        else
          klass.joins(:form)
            .where(forms: { secondary_organization_id: secondary_organization_id })
        end
      end

      def record_not_found(exception)
        render json: { errors: exception.message }, status: :not_found
      end

      def bad_request(exception)
        render json: { errors: exception.message }, status: :bad_request
      end
    end
  end
end
